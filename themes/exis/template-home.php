<?php
global $post;

/**
 * Template Name: Scroll Template - Home
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="content-wrapper" id="single-wrapper">
  <main class="site-main" id="main-site">
    <?php
      /* Header Home
      ----------------------------------------------------*/
      if ( have_posts() ) :
        while (have_posts()) : the_post();
          //Acf Hero area
          get_template_part('templates/page-header/header','hero');
        endwhile;
        wp_reset_query();
      endif;


      /* Scroll Nav
      ----------------------------------------------------*/
      get_template_part('templates/scroll-nav');


      /* Pages array
      ----------------------------------------------------*/
      $pages = "";

      if ( !empty($post) ):

        $args_home = array(
          'sort_order' => 'asc',
          'sort_column' => 'menu_order',
          'hierarchical' => 1,
          'exclude' => '',
          'include' => '',
          'meta_key' => '',
          'meta_value' => '',
          'child_of' => $post->ID,
          'parent' => -1,
          'depth' => 1 ,
          'exclude_tree' => '',
          'number' => '',
          'offset' => 0,
          'post_type' => 'page',
          'post_status' => 'publish'
        );

        $pages = get_pages($args_home);

      endif;


      // Get childs pages loop
      if(!empty($pages)):

        $page_counter = 0;

        foreach( $pages as $post ):

          setup_postdata($post);

          $slug = get_post_field('post_name');
          $page_id = get_the_ID();


          // get the template name
    			$template_name = get_post_meta( $page_id, '_wp_page_template', true );
          $nav_color = get_field('navigation_color', $page_id );
    			$template_name = str_replace(".php","", $template_name);

          //$ID counter
          $ID_counter = $page_counter++;

          echo '<div id="0'.$ID_counter.'" class="scroll-section page-'.$page_id.' page-'.$slug.'" data-nav-color="'.$nav_color.'" data-id="'.$page_id.'" data-slug="'.$slug.'" data-section="0'.$ID_counter.'" data-template="'.$template_name.'">';
            get_template_part($template_name);
          echo '</div><!-- .scroll-section -->';

        endforeach;

        wp_reset_postdata();

      endif;
    ?>

  </main><!-- #main -->
</div><!-- #single-wrapper -->

<?php
get_footer();
?>
