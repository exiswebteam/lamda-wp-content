<?php
/**
 * Template Name: Section - Project
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;


get_template_part('templates/section-header');
get_template_part('templates/content-project');
get_template_part('templates/section-footer');
?>
