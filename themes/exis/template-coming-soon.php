<?php
/**
 * Template Name: Coming Soon
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_header();
?>

<div class="content-wrapper" id="single-wrapper">
  <main class="site-main" id="main-site">

    <div class="hero-header">
      <div id="hero-slideshow" class="hero-slideshow">
        <article class="slide-item">
          <div class="img" style="background-image: url(<?= get_bloginfo('url'); ?>/wp-content/uploads/2020/05/1TALLER-e1590768298642.jpg);"></div>
        </article>
      </div>

      <div class="hero-pies">
        <section class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
              <h1 class="tw1">COMING <br><span>SOON</span></h1>
            </div>
          </div>
        </section>
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #single-wrapper -->

<?php
get_footer();
?>
