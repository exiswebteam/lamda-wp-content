<?php
/**
 * Template Name: Section - Sustainable
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;


get_template_part('templates/section-header');
get_template_part('templates/content-sustainable');
get_template_part('templates/section-footer');
?>
