<?php
/**
 * Template Name: Section - Human Capital
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_template_part('templates/section-header');
get_template_part('templates/content-human');
get_template_part('templates/section-footer');
?>
