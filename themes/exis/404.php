<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="hero-header mb4">
  <div id="hero-slideshow" class="hero-slideshow">
    <article class="slide-item">
      <div class="img" style="background-image: url(<?= get_bloginfo('url'); ?>/wp-content/uploads/2020/05/1TALLER-e1590768298642.jpg);"></div>
    </article>
  </div>

  <div class="hero-pies">
    <section class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
          <h1 class="tw1">404 ERROR <br><span>Page not found</span></h1>
          <p class="white desc tw1"><?php esc_html_e( 'It looks like nothing was found at this location.', 'understrap' ); ?></p>
          <div class="tw1">
            <?php get_search_form(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<div class="wrapper" id="error-404-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main">

					<section class="error-404 not-found">

						<header class="page-header">

							<h2 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'understrap' ); ?></h2>
						</header><!-- .page-header -->

						<div class="page-content">

              <?php get_search_form(); ?>


						</div><!-- .page-content -->

					</section><!-- .error-404 -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #error-404-wrapper -->

<?php
get_footer();
