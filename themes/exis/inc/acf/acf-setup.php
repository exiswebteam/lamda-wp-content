<?php
defined( 'ABSPATH' ) || exit;

/*==============================================================
    1. Register - CSS & JS within Admin area
================================================================*/
add_action('admin_enqueue_scripts', 'admin_style');

function admin_style()
{
  if(class_exists('acf'))
  {
    $the_theme = wp_get_theme();

    wp_enqueue_style('acf-admin-styles', get_stylesheet_directory_uri().'/inc/acf/css/admin-styles.css');
  }
}

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'EXIS® Theme',
		'menu_title'	=> 'EXIS® Theme',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


 /*==============================================================
     2. Hide ACF Admin on Staging/Production
 ================================================================*/
function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
        'https://lamda.exis.website', //staging
        'http://lamda.exis.website', //staging
        'https://www.lamdadev.com', //production
        'http://www.lamdadev.com', //production
        'https://www.lamdadev.com/annual-reports/2019',
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {
        // hide the acf menu item
        return false;
    } else {
        // show the acf menu item
        return true;
    }

}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');
