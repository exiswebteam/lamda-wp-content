<div id="lamda-preloader">
  <div class="logo-loader-wrap">
    <span class="logo-lamda"></span>
    <span class="logo-bottom"></span>
    <span class="square-1"></span>
    <span class="square-2"></span>
    <span class="text">LOADING</span>
  </div>
</div>
