<?php
/* Scrolling Nav
----------------------------------------------------*/
wp_nav_menu(
  array(
    'theme_location'  => 'primary',
    'container_class' => 'scroll-nav-wrap',
    'container_id'    => 'sections-numerical-nav',
    'menu_class'      => 'scroll-to-section',
    'fallback_cb'     => '',
    'menu_id'         => 'sections-main-menu',
    'depth'           => 1,
    'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
  )
);
?>
