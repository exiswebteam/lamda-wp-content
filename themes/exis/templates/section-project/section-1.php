<?php
//Hellinikon Project section
$post_id = get_the_ID();
$section = get_post_field( 'menu_order', $post_id);
$bg_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$section_title = get_the_title();
$secondary_title = get_field('title_secondary');
$section_subtitle = get_field('subtitle');
$section_subtitle2 = get_field('secondary_subtitle');
?>

<div class="inner-section-1 on-viewport ovf-hidden" style="background-image: url(<?= $bg_image[0]; ?>);">

  <div class="header-wrap ovf-hidden">

    <hgroup class="container">
      <h3 class="section-title white x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px) scale(0.1);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= (!empty($secondary_title))? $secondary_title : $section_title; ?></h3>
      <h4 class="section-subtitle white x-s1-2" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $section_subtitle; ?></h4>
      <h6 class="section-num white left x-s1-0" data-bottom-top="transform:translateY(80px) scale(0.2);" data-center-center="transform:translateY(10px) scale(1);">0<?= $section; ?></h6>
    </hgroup>

    <div class="parallax-wrapper x-op-0">
      <div class="container" data-bottom-top="transform:translateY(20%); opacity:0;" data-center-center="transform:translateY(0); opacity:1;">
        <div class="row">
          <div class="col-10 col-sm-12 tc col-md-10 col-lg-7 ml-auto mr-auto">
            <div class="desc desc-parallax">
              <?= get_post_field('post_content', $post_id); ?>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div><!--.inner-section-1-->

<div class="inner-section-1-bottom on-viewport pt4 pb4 ovf-hidden">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-10 col-lg-7 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateX(-30%);" data-center-center="opacity: 1; transform: translateX(0);">
        <h3 class="title-3 blue x-op-0"><?= $section_subtitle2; ?></h3>
        <div class="desc mb3 x-op-0">
          <?= get_field('description'); ?>
        </div>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-7 ml-auto mr-auto">
        <div class="icon-wrapper x-op-0">
          <?php
          if( have_rows('add_icons') ):
            while ( have_rows('add_icons') ) : the_row();
            $title = get_sub_field('title');
            $icon = get_sub_field('icon');
            $desc = get_sub_field('description');
          ?>
          <article class="item x-op-0">
            <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="transform: scale(0);" data-center-center="transform: scale(1);">
            <h5 class="title-3 blue" data-bottom-top="opacity: 0.4; " data-center-center="opacity: 1;"><?= $title; ?></h5>
            <div class="wrap-desc blue-light" data-bottom-top="opacity: 0.4;" data-center-center="opacity: 1;">
              <?= $desc; ?>
            </div>
          </article>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
