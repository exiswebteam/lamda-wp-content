<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$description = get_sub_field('description');
?>
<div class="inner-section-4 on-viewport ovf-hidden pb1" data-fx="landfill_waste_data">

  <header class="container tc">
    <h3 class="blue text-2 mb2 s1-2"><?= $title; ?></h3>
  </header>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8 ml-auto mr-auto">
        <div class="row">
          <div class="row waste-data-repeater">
            <?php
            if( have_rows('add_icons') ):
              $count_waste_data = 0;
              while ( have_rows('add_icons') ) : the_row();
              $title = get_sub_field('title');
              $desc = get_sub_field('subtitle');
            ?>
            <article class="col-md-4 item mb2 tc x-op-<?= $count_waste_data++; ?>" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
              <h5 class="blue-light title-3"><?= $title; ?></h5>
              <div class="wrap-desc desc">
                <?= $desc; ?>
              </div>
            </article>
            <?php
              endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if(!empty($description)): ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6 ml-auto mr-auto">
        <div class="desc tc mb2 s1-3">
          <?= $description; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div><!-- .inner-section-4 -->
