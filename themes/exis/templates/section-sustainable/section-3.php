<?php
//SUSTAINABLE section
?>
<div class="inner-section-3 on-viewport mb2 ovf-hidden" data-fx="landfill_waste_icon">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8 ml-auto mr-auto">
      <?php
      if( have_rows('waste_icon') ):
        $count_waste_items = 0;
        while ( have_rows('waste_icon') ) : the_row();
        $title = get_sub_field('title');
        $subtitle = get_sub_field('subtitle');
        $waste_icon = get_sub_field('svg_graph_icon');
      ?>
      <article class="item mb2 tc x-op-<?= $count_waste_items++; ?>" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
        <h5 class="title-3 blue"><?= $title; ?></h5>
        <h6 class="desc blue-secondary mb1"><?= $subtitle; ?></h6>
        <div class="waste-svg" >
          <?php
          if(!empty($waste_icon)):
            //SVG IMAGE
            if (strpos($waste_icon['url'], '.svg') !== false)
            {
              get_template_part('templates/svg/svg-waste');
            }
          endif;
          ?>
        </div>
      </article>
      <?php
        endwhile;
      endif;
      ?>
      </div>
    </div>
  </div>
</div><!-- .inner-section-3 -->
