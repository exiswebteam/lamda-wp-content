<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$desc = get_sub_field('description');
?>
<div class="inner-section-6 on-viewport ovf-hidden pt4 pb1" data-fx="initiatives_section">
  <header class="container tc">
    <h4 class="blue mb4 x-s1-1" data-bottom-top="opacity:0; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h4>
  </header>

  <?php if(!empty($desc)): ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-10 col-lg-8 ml-auto mr-auto">
        <div class="desc tc mb2 x-s1-2" data-bottom-top="opacity:0; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);">
          <?= $desc; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-8 ml-auto mr-auto">
        <div class="row">
          <?php
          if( have_rows('add_ngos') ):
            $count_ngos = 0;
            while ( have_rows('add_ngos') ) : the_row();
            $icon = get_sub_field('icon');
            $desc = get_sub_field('description');
          ?>
          <div class="col-sm-6 col-md-4 item mb3 x-op-<?= $count_ngos++; ?>">
            <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="opacity:0; transform:translateY(50%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
            <div class="wrap-desc desc" data-bottom-top="opacity:0.5; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);">
              <?= $desc; ?>
            </div>
          </div>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- .inner-section-6-->
