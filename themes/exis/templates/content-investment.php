<?php
/*
use class .investment-content-wrap for css */
?>
<div class="investment-content-wrap page-section">
  <?php
  //The content
  get_template_part('templates/section-investment/section-1');

  //Fx content area
  if( have_rows('add_section_investment') ):

      while ( have_rows('add_section_investment') ) : the_row();

      if( get_row_layout() == 'group_ebitda' ): get_template_part('templates/section-investment/section-2');
      elseif( get_row_layout() == 'total_turnover_of_malls' ): get_template_part('templates/section-investment/section-3');
      elseif( get_row_layout() == 'parallax_image' ): get_template_part('templates/section-investment/section-4');
      elseif( get_row_layout() == 'icons' ): get_template_part('templates/section-investment/section-5');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .investment-content-wrap -->
