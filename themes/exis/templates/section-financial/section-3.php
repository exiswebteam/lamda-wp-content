<?php
//FINANCIAL section
$share_bg_image = get_sub_field('background_image');
$share_title = get_sub_field('title');
$share_desc = get_sub_field('content_top');
$share_desc2 = get_sub_field('content_bottom');
$share_table = get_sub_field('table');
?>
<div class="inner-section-3 fixed on-viewport pt4 pb4 section-parallax" data-fx="share_capital_increase" style="background-image: url(<?php echo $share_bg_image['url']; ?>);">
  <header class="container">
    <h4 class="section-subtitle white x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);">
      <?= $share_title; ?>
    </h4>
  </header>

  <div class="ovf-hidden">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="desc white tc x-s1-2" data-bottom-top="opacity:0; transform:translateX(-30%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?= $share_desc; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="desc white mb2 tc x-s1-3" data-bottom-top="opacity:0; transform:translateX(30%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?= $share_desc2; ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 col-md-7 ml-auto mr-auto">
          <div class="data-table mb4 x-s1-3" data-bottom-top="opacity:0; transform:translateX(-30%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?= $share_table; ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 col-md-7 ml-auto mr-auto">
          <div class="charts-wrap on-viewport">
            <?php
            if( have_rows('charts_data') ):
                while ( have_rows('charts_data') ) : the_row();

                    if( get_row_layout() == 'charts' ):
                        get_template_part('templates/section-financial/charts/charts-data');

                    elseif( get_row_layout() == 'chart_nav' ):
                        get_template_part('templates/section-financial/charts/chart-nav');

                    endif;

                endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div><!--.container-->
  </div>
</div><!--.inner-section-3-->
