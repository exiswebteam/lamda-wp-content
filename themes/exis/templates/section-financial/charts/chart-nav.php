<nav class="chart-nav">
  <?php
  if( have_rows('chart_color_nav') ):
    while ( have_rows('chart_color_nav') ) : the_row();
    $color = get_sub_field('color');
    $title = get_sub_field('title');
  ?>
  <div class="nav-item">
    <span class="square" style="background-color: <?= $color; ?>;"></span>
    <span class="title white"><?= $title; ?></span>
  </div>
  <?php
    endwhile;
  endif;
  ?>
</nav>
