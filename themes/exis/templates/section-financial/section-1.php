<?php
//FINANCIAL section
$post_id = get_the_ID();
$section = get_post_field( 'menu_order', $post_id);
$bg_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$section_title = get_the_title();
$secondary_title = get_field('title_secondary');
$section_subtitle = get_field('subtitle');
$parallax_chart = get_field('parallax_image');
?>

<div id="financial-chart" class="chart-1 on-viewport parallax-chart x-s01-chart" data-bottom-top="top:-160px;" data-top-bottom="top: 80px;">
  <?php
  if(!empty($parallax_chart)):
    //SVG IMAGE
    if (strpos($parallax_chart['url'], '.svg') !== false)
    {
      get_template_part('templates/svg/svg-chart');
    }
  endif;
  ?>
</div>

<div class="inner-section-1 on-viewport" style="background-image: url(<?= $bg_image[0]; ?>);">

  <div class="header-wrap ovf-hidden">

    <hgroup class="container">
      <h3 class="section-title white x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px) scale(0.1);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= (!empty($secondary_title))? $secondary_title : $section_title; ?></h3>
      <h4 class="section-subtitle white x-s1-2" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $section_subtitle; ?></h4>
      <h6 class="section-num white left x-s1-0" data-bottom-top="transform:translateY(80px) scale(0.2);" data-center-center="transform:translateY(10px) scale(1);">0<?= $section; ?></h6>
    </hgroup>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="desc white x-s1-3" data-bottom-top="opacity:0; transform:translateX(-40%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?= get_post_field('post_content', $post_id); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!--.inner-section-1 -->
