<?php
//FINANCIAL section
$icon = get_sub_field('icon');
$secondary_title = get_sub_field('title');
?>
<div class="inner-section-2 on-viewport" data-fx="numerical_figures">
  <header class="header-wrap container ovf-hidden">
    <img class="x-icon-rotate" data-bottom-top="transform:rotate(-80deg);" data-top-bottom="transform:rotate(0deg);" src="<?= $icon['url']; ?>" alt="<?= $secondary_title; ?>">
    <h4 class="section-subtitle blue x-op-1" data-bottom-top="opacity:0; transform:translateY(80px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $secondary_title; ?></h4>
  </header>

  <div class="container numerical-wrap">
    <div class="row">
      <?php
      if( have_rows('numerical_figures_repeater') ):
        $count_numerical_figures = 0;
        while ( have_rows('numerical_figures_repeater') ) : the_row();

          //Increase
          $title = get_sub_field('value_1');
          $subtitle = get_sub_field('subtitle');
          $desc = get_sub_field('description');

          //Revenue
          $title2 = get_sub_field('value_2');
          $desc2 = get_sub_field('revenue_description');

          $arrow = (empty($title2))? ' left-arrow' : ' down-arrow';
          ?>
          <div class="col-sm-4 col-md-4 mb4 item <?= $arrow; ?> op-<?= $count_numerical_figures++; ?>">
            <div class="wrap-data">
              <div class="first">
                <?php if(!empty($title)): ?>
                  <span class="arrow-1 s01-arr hide"></span>
                  <h5><?= $title; ?></h5>
                <?php endif; ?>

                <?php if(!empty($subtitle)): ?>
                  <div class="subtitle">
                    <?= $subtitle; ?>
                  </div>
                <?php endif; ?>

                <?php if(!empty($desc)): ?>
                  <div class="desc">
                    <?= $desc; ?>
                  </div>
                <?php endif; ?>
              </div>

              <?php if(!empty($title2)): ?>
              <span class="arrow-2 x-s01-arr hide" data-bottom-top="transform:translateY(5px);" data-top-bottom="transform:translateY(0);"></span>
              <div class="second">
                <h5><?= $title2; ?></h5>

                <?php if(!empty($desc)): ?>
                  <div class="desc">
                    <?= $desc2; ?>
                  </div>
                <?php endif; ?>
              </div>
              <?php endif; ?>
            </div>
          </div><!--.item-->
        <?php
        endwhile;
      endif;
      ?>
    </div>
  </div>
</div><!-- .inner-section-2 -->
