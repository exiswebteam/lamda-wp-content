<?php
//INVESTMENT section
$post_id = get_the_ID();
$section = get_post_field( 'menu_order', $post_id);
$section_title = get_the_title();
$secondary_title = get_field('title_secondary');
$section_subtitle = get_field('subtitle');
?>

<div class="inner-section-1 on-viewport" data-fx="">

  <div class="header-wrap ovf-hidden">

    <hgroup class="container">
      <h3 class="section-title blue x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px) scale(0.1);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= (!empty($secondary_title))? $secondary_title : $section_title; ?></h3>
      <h4 class="section-subtitle blue x-s1-2" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $section_subtitle; ?></h4>
      <h6 class="section-num left x-s1-0" data-bottom-top="transform:translateY(80px) scale(0.2);" data-center-center="transform:translateY(10px) scale(1);">0<?= $section; ?></h6>
    </hgroup>

    <div class="container ovf-hidden">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="desc x-s1-3" data-bottom-top="opacity:0; transform:translateX(40%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?= get_post_field('post_content', $post_id); ?>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-7 ml-auto mr-auto">
        <div class="investment-pie x-op-0 mb4">
          <?php
          if( have_rows('pie_data') ):
            while ( have_rows('pie_data') ) : the_row();

              //Increase
              $title = get_sub_field('value');
              $icon = get_sub_field('icon');
              $desc = get_sub_field('description');
              ?>
              <article class="item ovf-hidden">
                <div class="col-wrap col-second">
                  <?php
                  if(!empty($icon)):
                    echo '<img width="130" src="'.$icon['url'].'" alt="'.$desc.'">';
                  else:
                    echo '<div class="fullpie-wrapper">
                      <h5 class="blue-light title-3">'.$title.'</h5>
                      <div class="fullpie piespinner"></div>
                      <div class="fullpie piefiller"></div>
                      <div class="piemask"></div>
                    </div>';
                  endif;
                  ?>
                </div>

                <div class="col-wrap col-third desc" data-bottom-top="opacity:0;" data-center-center="opacity: 1;">
                  <?= $desc; ?>
                </div>
              </article>
              <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>

</div><!--.inner-section-1-->
