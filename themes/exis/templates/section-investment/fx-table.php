<div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-7 ml-auto mr-auto">
      <div class="inverstment-table-wrap on-viewport">
        <?php
        if( have_rows('add_data_table') ):
          while ( have_rows('add_data_table') ) : the_row();

            if( get_row_layout() == 'data_table' ):
                get_template_part('templates/section-investment/tables/data-table');

            //elseif( get_row_layout() == '' ): get_template_part('templates/section-investment/');

            endif;

          endwhile;
        endif;
        ?>
      </div>
    </div>
  </div>
</div>
