<?php
$width = strtolower(get_sub_field('width'));
$title = get_sub_field('title');
?>

<div class="chart-item op-0 col-<?= $width; ?>">
  <div class="chart-wrap">
    <h5><?= $title; ?></h5>
    <ul>
    <?php
    if( have_rows('add_chart_data') ):
      while ( have_rows('add_chart_data') ) : the_row();
      $color = get_sub_field('color');
      $val = get_sub_field('value');
      $counter = get_sub_field('counter');
      $pos = strtolower(get_sub_field('position'));
      $bar_width = (!empty($val))? ((int)$val) * 0.66 : 100;
    ?>
    <li>
      <?php if($pos=='left'): ?>
        <div class="val <?= $pos; ?>" style="color: <?= $color; ?>;"><?= $counter; ?></div>
      <?php endif; ?>
      <div class="bar" style="background-color: <?= $color; ?>; width: <?= $bar_width; ?>%;" data-val="<?= $val; ?>"></div>
      <?php if($pos=='right'): ?>
        <div class="val <?= $pos; ?>" style="color: <?= $color; ?>;"><?= $counter; ?></div>
      <?php endif; ?>
    </li>
    <?php
      endwhile;
    endif;
    ?>
    </ul>
  </div>
</div>
