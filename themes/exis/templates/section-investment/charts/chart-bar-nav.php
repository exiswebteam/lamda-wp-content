<nav class="chart-nav">

  <?php
  if( have_rows('add_chart_nav') ):
    while ( have_rows('add_chart_nav') ) : the_row();
    $color = get_sub_field('color');
    $title = get_sub_field('year');
  ?>
  <div class="nav-item op-1">
    <span class="square" style="background-color: <?= $color; ?>;"></span>
    <span class="title black"><?= $title; ?></span>
  </div>
  <?php
    endwhile;
  endif;
  ?>
</nav>
