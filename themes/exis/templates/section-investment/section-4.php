<?php
//INVESTMENT section
$pos = strtolower(get_sub_field('image_direction'));
$bg_image = get_sub_field('image');

$title = get_sub_field('title');
$desc = get_sub_field('description');
$more_desc = get_sub_field('more_content');
$btn_more = get_sub_field('button_more');
$btn_less = get_sub_field('button_less');

$ID = str_replace(' ', '', strtolower($title));
?>
<div class="inner-section-4 ovf-hidden pos-<?= $pos; ?> on-viewport item-<?= $ID; ?>" data-fx="parallax_image">
  <div class="parallax-image" style="background-image: url(<?= $bg_image['url']; ?>);">

    <article class="parallax-description">
      <h4 class="title black"><?= $title; ?></h4>
      <div class="desc">
        <?= $desc; ?>
      </div>
      <?php if(!empty($more_desc)): ?>
      <div id="<?= $ID; ?>" class="desc hide">
        <?= $desc; ?>
      </div>
      <nav class="tc">
        <a href="#<?= $ID; ?>" class="read-more" data-less="<?= $btn_less; ?>" data-more="<?= $btn_more; ?>"><?= $btn_more; ?></a>
      </nav>
      <?php endif; ?>
    </article>

  </div>
</div><!-- .inner-section-4 -->
