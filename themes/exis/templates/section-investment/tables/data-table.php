<?php
$icon = get_sub_field('icon');
$title = get_sub_field('title');
$table = get_sub_field('table');
?>

<article class="table-item mb3">
  <header class="table-section tc mb2">
    <img class="x-icon-rotate"  data-bottom-top="transform:rotate(-80deg);" data-center-center="transform:rotate(0deg);" src="<?= $icon['url']; ?>" alt="<?= $title; ?>">
    <h4 class="blue"><?= $title; ?></h4>
  </header>
  <div class="data-table x-op-0 desc" data-bottom-top="transform:scale(0.5);" data-center-center="transform:scale(1);">
    <?= $table; ?>
  </div>
</article>
