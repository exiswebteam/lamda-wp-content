<?php
//INVESTMENT section
?>
<div class="inner-section-5 on-viewport pb2" data-fx="icons">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-9 ml-auto mr-auto">
        <div class="row">
          <?php
          if( have_rows('add_icons') ):
            $icon_count = 1;
            while ( have_rows('add_icons') ) : the_row();
              $title = get_sub_field('title');
              $icon = get_sub_field('icon');
              $desc = get_sub_field('description');
              ?>
              <div class="col-item col-xs-6 col-sm-6 col-md-3">
                <div class="item-wrap ovf-hidden">
                  <article class="item x-op-0 x-item-<?= $icon_count++; ?>" data-bottom-top="opacity:0.1; transform:scale(0.1);" data-center-center="opacity: 1; transform:scale(1);">
                    <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>">
                    <h5 class="title-3 blue"><?= $title; ?></h5>
                    <div class="wrap-desc blue-light">
                      <?= $desc; ?>
                    </div>
                  </article>
                </div>
              </div>
            <?php
          endwhile;
        endif;
        ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- .inner-section-5 -->
