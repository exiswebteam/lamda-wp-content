<div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-10 ml-auto mr-auto">
      <div class="investment-mall-chart on-viewport">
        <?php
        if( have_rows('charts') ):
          while ( have_rows('charts') ) : the_row();

            if( get_row_layout() == 'add_chart_bar' ):
                get_template_part('templates/section-investment/charts/chart-bar');

            elseif( get_row_layout() == 'chart_nav' ):
                get_template_part('templates/section-investment/charts/chart-bar-nav');

            endif;

          endwhile;
        endif;
        ?>
      </div>
    </div>
  </div>
</div>
