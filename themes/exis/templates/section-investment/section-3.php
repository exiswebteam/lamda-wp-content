<?php
//INVESTMENT section
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$bg_color = get_sub_field('background_color');
?>
<div class="inner-section-3 on-viewport ovf-hidden pt4 pb2" data-fx="total_turnover_of_malls">
  <div class="investment-tables">
    <hgroup class="table-header tc mb4 x-op-0">
      <h4 class="blue" data-bottom-top="opacity:0.2; transform:translateY(-30px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h4>
      <h5 class="desc" data-bottom-top="opacity:0; transform:translateY(20px);" data-center-center="opacity: 1; transform:translateY(0);">
        <?= $subtitle; ?>
      </h5>
    </hgroup>

    <?php
    get_template_part('templates/section-investment/fx-mall-charts');
    get_template_part('templates/section-investment/fx-table');
    ?>

  </div>
</div><!-- .inner-section-3 -->
