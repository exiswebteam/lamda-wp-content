<?php
/*
use class .project-content-wrap for css */
?>
<div class="project-content-wrap page-section">
  <?php
  //The content
  get_template_part('templates/section-project/section-1');

  //Fx content area
  if( have_rows('add_section_project') ):

      while ( have_rows('add_section_project') ) : the_row();

      if( get_row_layout() == 'icons' ): get_template_part('templates/section-project/section-2');
      elseif( get_row_layout() == 'smart_city' ): get_template_part('templates/section-project/section-3');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .project-content-wrap -->
