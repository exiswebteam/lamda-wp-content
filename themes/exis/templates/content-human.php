<?php
/*
use class .capital-content-wrap for css */
?>
<div class="capital-content-wrap page-section">
  <?php
  //The content
  get_template_part('templates/section-human/section-1');

  //Fx content area
  if( have_rows('add_section_human') ):

      while ( have_rows('add_section_human') ) : the_row();

      if( get_row_layout() == 'add_icons' ): get_template_part('templates/section-human/section-2');
      elseif( get_row_layout() == 'clock' ): get_template_part('templates/section-human/section-3');
      elseif( get_row_layout() == 'financial' ): get_template_part('templates/section-human/section-4');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .capital-content-wrap -->
