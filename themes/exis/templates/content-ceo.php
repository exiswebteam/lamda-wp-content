<?php
$post_id = get_the_ID();
$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$title = get_the_title();
$secondary_title = get_field('title_secondary');
?>

<div class="picture-card-section on-viewport">

  <div class="picture-card width-30-70">

    <div class="picture-card-img-wrap ovf-hidden">
      <?php if(!empty($img)): ?>
      <img class="x-a1" src="<?= $img[0]; ?>" alt="<?= $title; ?>" data-bottom-top="opacity:1; transform:translateX(-30%);" data-center-center="opacity: 1; transform:translateX(0);">
      <?php endif; ?>
    </div>

    <div class="picture-card-content-wrap">

      <div class="picture-card-content-inner">

        <h2 class="title x-a2" data-bottom-top="opacity:0; transform:translateY(10%);" data-center-center="opacity: 1; transform:translateY(0);"><?= (!empty($secondary_title))? $secondary_title : $title; ?></h2>
        <div class="content desc x-a3" data-bottom-top="opacity:0; transform:translateY(15%);" data-center-center="opacity: 1; transform:translateY(0);">
          <?= get_post_field('post_content', $post_id); ?>
        </div>

        <div class="more-content desc hide" id="read-more-ceo">
          <?= get_field('ceo_more_content'); ?>
        </div>


        <div class="picture-card-cta tc cta-btn-wrap a4">
          <a href="#read-more-ceo" class="read-more" data-less="<?= get_field('button_less'); ?>" data-more="<?= get_field('button_more'); ?>"><?= get_field('button_more'); ?></a>
        </div>

      </div>

    </div><!-- end of picture-card-content-wrap -->

  </div><!-- end of picture-card -->


</div><!-- end of picture-card-section -->
