<svg width="103px" height="106px" viewBox="0 0 103 106" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Welcome-4" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Desktop-4" transform="translate(-395.000000, -4763.000000)">
            <g id="Group-31" transform="translate(399.000000, 4766.000000)">
                <path d="M45,0 L45,51 L94,51 C94,51 93.2269951,7.30127429 45,0" id="Fill-3" fill="#4A90E2"/>
                <path d="M95,49.9989535 C95,77.0605241 73.7337154,99 47.5,99 C21.2662846,99 0,77.0605241 0,49.9989535 C0,22.937383 21.2662846,1 47.5,1 C73.7337154,1 95,22.937383 95,49.9989535 Z" id="Stroke-1" stroke="#9B9B9B" stroke-width="6.75400019" stroke-linecap="round" stroke-linejoin="round"/>
            </g>
        </g>
    </g>
</svg>
