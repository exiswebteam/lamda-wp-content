<?php
while ( have_posts() ) : the_post();

$subtitle = get_field('page_title');

if(!empty($subtitle)):
?>
<div class="page-header">
  <section class="container">
    <h1><?= $subtitle; ?></h1>
  </section>
</div>
<?php endif; ?>
<?php endwhile; ?>
