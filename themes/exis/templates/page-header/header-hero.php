<?php
$image = get_field('image');
$title = get_field('title');
?>

<div class="hero-header">

  <div id="hero-slideshow" class="hero-slideshow">
    <?php
    if( have_rows('slideshow') ):
      while ( have_rows('slideshow') ) : the_row();
        $slide = get_sub_field('image');
        ?>
        <div class="slide-item">
          <div class="img" style="background-image: url(<?= $slide['url']; ?>);"></div>
        </div>
      <?php
      endwhile;
    endif;
    ?>
  </div>

  <div class="hero-pies">
    <section class="container">
      <h1 class="tw1"><?= $title; ?></h1>
        <?php
        if( have_rows('add_pie') ):
          echo '<div class="pie-wrap">';
          $i = 1;
          while ( have_rows('add_pie') ) : the_row();
            $pie_val = get_sub_field('value');
            $pie_title = get_sub_field('title');
            ?>
            <div class="pie-item item-<?= $i++; ?>">
              <div class="pie-wrap">
                <div class="pie hero-pie-init" data-val="<?= $pie_val; ?>"></div>
                <span class="val">
                  <span class="numscroller" data-min="0" data-max="<?= $pie_val; ?>" data-delay="1" data-increment="1"><?= '0'; ?></span>%
                </span>
              </div>
              <h2 class="tw2"><?= $pie_title; ?></h2>
            </div>
          <?php
          endwhile;

          echo '</div>';
        endif;
        ?>
    </section>
  </div>

  <a href="#00" class="tw3 scroll-to scroll-arrow" data-offset="0"></a>
</div>
