<?php
//HUMAN CAPITAL section
?>
<div class="inner-section-3 on-viewport ovf-hidden pt1 pb1" data-fx="clock">
  <div class="container" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);">
    <div class="row">
      <div class="col-sm-12 col-md-7 ml-auto mr-auto">
        <div class="clock-repeater-wrap">
          <?php
          if( have_rows('add_clock') ):
            $count_clocks = 0;
            while ( have_rows('add_clock') ) : the_row();
            $title = get_sub_field('title');
            $icon = get_sub_field('icon');
            $subtitle = get_sub_field('subtitle');
          ?>
          <div class="item mb2 x-op-<?= $count_clocks++; ?>">
            <?php
            if(!empty($icon)):
            ?>
            <div class="item-1 clock icon">
              <?php
              if(!empty($icon)):
                //SVG IMAGE
                if (strpos($icon['url'], '.svg') !== false)
                {
                  get_template_part('templates/svg/svg-clock');
                } else {
                  echo '<img src="'.$icon['url'].'" alt="'.get_the_title().'">';
                }
              endif;
              ?>
            </div>
            <?php endif; ?>

            <div class="item-2">
              <h3 class="title-2 blue-light"><?= $title; ?></h3>
              <h4 class="title-clock blue"><?= $subtitle; ?></h4>
            </div>
          </div><!--.item-->
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>

</div><!--.inner-section-3-->
