<?php
//HUMAN CAPITAL section
$title = get_sub_field('title');
$svg = get_sub_field('svg_graph');
$bg_color = get_sub_field('background_color');
$desc = get_sub_field('description');
?>
<div class="inner-section-4 on-viewport pt4 pb4" data-fx="financial" style="background-color: <?= $bg_color; ?>;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-7 ml-auto mr-auto">
        <div class="financial-wrap">
          <div class="item-1" data-bottom-top="opacity:0; transform:translateX(-50%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?php
            if(!empty($svg)):
              //SVG IMAGE
              if (strpos($svg['url'], '.svg') !== false)
              {
                get_template_part('templates/svg/svg-hand');
              }
            endif;
            ?>
          </div>
          <div class="item-2 x-op-0" data-bottom-top="opacity:0; transform:translateX(50%);" data-center-center="opacity: 1; transform:translateX(0);">
            <h3 class="blue"><?= $title; ?></h3>
            <div class="desc">
              <?= $desc; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div><!--.inner-section-4-->
