<?php
//HUMAN CAPITAL section - Pies
?>
<div class="inner-section-2 on-viewport ovf-hidden pt1 pb1" data-fx="add_icons">
  <div class="container" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);">
    <div class="row">
      <div class="col-sm-12 col-md-8 ml-auto mr-auto">
        <div class="pies-repeater-wrap columns-<?= $cols; ?>">
          <?php
          if( have_rows('icons') ):
            $count_icons = 0;
            while ( have_rows('icons') ) : the_row();
            $counter = $count_icons++;
            $title = get_sub_field('title');
            $subtitle = get_sub_field('subtitle');
            $icon = get_sub_field('icon');
            $pie_bg = get_sub_field('pie_bg');
            $val = get_sub_field('pie_value');
            $secondary = get_sub_field('secondary_title');
            $align_text = get_sub_field('align');
            $color_1 = get_sub_field('color_1');
            $color_2 = get_sub_field('color_2');
            $bgimg_icon = (!empty($pie_bg))? ' style="background-image: url('.$pie_bg['url'].');"': '';

            $active_pie = (!empty($val))? 'true' : 'false';
          ?>
          <article class="item mb2 tc item-<?= $align_text; ?> active-pie-<?= $active_pie; ?> item-<?= $counter; ?>">

            <?php if(!empty($title)): ?>
            <h4 class="title-3 blue-light op-<?= $counter; ?>"><?= $title; ?></h4>
            <?php endif; ?>

            <?php if(!empty($subtitle)): ?>
            <h5 class="subtitle blue mb1 op-<?= $counter; ?>"><?= $subtitle; ?></h5>
            <?php endif; ?>

            <?php if(!empty($icon)): ?>
            <img class="mb1 op-<?= $counter; ?>" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>">
            <?php endif; ?>

            <?php if(!empty($val)): ?>
            <div class="pie human-capital-pie" data-color-1="<?= (!empty($color_1))? $color_1: '#F1F1F1'; ?>" data-color-2="<?= (!empty($color_2))? $color_2: '#F9F9F9'; ?>" data-val="<?= $val; ?>"<?= $bgimg_icon; ?>></div>
            <?php endif; ?>

            <?php if(!empty($secondary)): ?>
            <div class="wrap-desc blue-light desc op-<?= $counter; ?>">
              <?= $secondary; ?>
            </div>
            <?php endif; ?>
          </article>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>

</div><!--.inner-section-2-->
